#!/usr/bin/env ruby

require 'fileutils'
require 'json'

SCAN_DIR='scans'

IMAGES = [
  'almalinux:8.4',
  'alpine:3.12.0',
  'amazonlinux:2.0.20201218.1',
  'centos:8',
  'debian:buster-20210511',
  'gcr.io/distroless/base-debian9:latest',
  'opensuse/leap:15.0',
  'registry',
  'oraclelinux:8.2',
  'photon:1.0-20210409',
  'registry.access.redhat.com/rhel7:7.9-333',
  'rockylinux:8.5',
  'redhat/ubi8:8.2-299',
  'ubuntu:bionic-20210222',
  'registry.gitlab.com/gitlab-org/security-products/dast/webgoat-8.0@sha256:bc09fe2e0721dfaeee79364115aeedf2174cce0947b9ae5fe7c33312ee019a4e',
  'mcr.microsoft.com/cbl-mariner/base/core:2.0',
  'cgr.dev/chainguard/go:latest'
]

def main
  FileUtils.mkdir_p(SCAN_DIR)

  IMAGES.each do |image|
    image_name = image.gsub('/', '-')
    trivy_sbom = File.join(SCAN_DIR, "#{image_name}-trivy.cdx.json")
    syft_sbom = File.join(SCAN_DIR, "#{image_name}-syft.cdx.json")

    puts "Generating SBOMs"
    generate_sboms(image, trivy_sbom, syft_sbom)

    puts "Comparing SBOMs"
    compare_sboms(trivy_sbom, syft_sbom)
  end
end

def compare_sboms(trivy_sbom, syft_sbom)
  parsed_trivy = JSON.parse(File.read(trivy_sbom))
  parsed_syft = JSON.parse(File.read(syft_sbom))

  trivy_components = parsed_trivy.dig('components').map { |c| c['name'] }.sort
  syft_components = parsed_syft.dig('components').map { |c| c['name'] }.sort

  components_only_in_syft = syft_components - trivy_components
  components_only_in_trivy = trivy_components - syft_components

  if components_only_in_syft == components_only_in_trivy
    return
  end

  puts "Comparing #{trivy_sbom} vs #{syft_sbom}"
  puts "  Components only in Syft: #{components_only_in_syft}"
  puts "  Components only in Trivy: #{components_only_in_trivy}"
end

def generate_sboms(image, trivy_sbom, syft_sbom)
  unless File.exist?(trivy_sbom)
    `trivy image --format cyclonedx #{image} > #{trivy_sbom}`
  end

  unless File.exist?(syft_sbom)
    `syft #{image} -o cyclonedx-json > #{syft_sbom}`
  end

  [trivy_sbom, syft_sbom].each do |sbom|
    puts "Number of components for #{sbom}:"
    num_components = `jq '.components | length' #{sbom}`.chomp
    puts "  #{num_components}"
  end
end

main
